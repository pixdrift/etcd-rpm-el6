%global debug_package	%{nil}
%global provider	github
%global provider_tld	com
%global project		coreos
%global repo		etcd

%global import_path	%{provider}.%{provider_tld}/%{project}/%{repo}

Name:		%{repo}
Version:	2.2.0
Release:	2%{?dist}
Summary:	A highly-available key value store for shared configuration
License:	ASL 2.0
URL:		https://%{import_path}
Source0:	https://%{import_path}/archive/v%{version}.tar.gz
Source1:	%{name}.init
Source2:	%{name}.sysconfig
Source3:	%{name}.logrotate

ExclusiveArch:	%{ix86} x86_64 %{arm}
BuildRequires:	golang >= 1.5.1-0
Requires(pre):	shadow-utils
Requires(post): chkconfig
Requires(preun): chkconfig initscripts 
Requires(postun): initscripts
Requires: logrotate

%description
A highly-available key value store for shared configuration.

%prep
%setup -qn %{name}-%{version}

%build
./build

%install
install -D -p -m 0755 bin/%{name} %{buildroot}%{_bindir}/%{name}
install -D -p -m 0755 bin/%{name}ctl %{buildroot}%{_bindir}/%{name}ctl
install -D -p -m 0755 %{S:1} %{buildroot}%{_initrddir}/%{name}
install -D -m 0644 %{S:2} %{buildroot}%{_sysconfdir}/sysconfig/%{name}
install -D -m 0644 %{S:3} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
install -d -m 0755 %{buildroot}%{_sharedstatedir}/%{name}

%check

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{_sharedstatedir}/%{name} \
	-s /sbin/nologin -c "etcd user" %{name}

%post
/sbin/chkconfig --add %{name}

%preun
if [ $1 = 0 ]; then # package is being erased, not upgraded
	/sbin/service %{name} stop > /dev/null 2>&1
	/sbin/chkconfig --del %{name}
fi

%postun
if [ $1 = 1 ]; then # package is being upgraded
	/sbin/service %{name} condrestart > /dev/null 2>&1
fi

%files
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%{_bindir}/%{name}
%{_bindir}/%{name}ctl
%{_initrddir}/%{name}
%dir %attr(-,%{name},%{name}) %{_sharedstatedir}/%{name}
%doc LICENSE README.md Documentation/internal-protocol-versioning.md
%doc Godeps/Godeps.json

%changelog
* Tue Sep 15 2015 PixelDrift.NET <support@pixeldrift.net> - 2.2.0-2
- Update static golang to 1.5.1 using koji build for el6

* Tue Sep 15 2015 PixelDrift.NET <support@pixeldrift.net> - 2.2.0-1
- Update to v2.2.0

* Tue Sep 15 2015 PixelDrift.NET <support@pixeldrift.net> - 2.1.3-1
- Update to v2.1.3

* Tue Sep 15 2015 PixelDrift.NET <support@pixeldrift.net> - 2.1.2-1
- Update to v2.1.2

* Sun Jul 26 2015 PixelDrift.NET <support@pixeldrift.net> - 2.1.1-1
- Update to v2.1.1

* Thu Jul 02 2015 PixelDrift.NET <support@pixeldrift.net> - 2.1.0rc.0-1
- Update to v2.1.0-rc.0

* Sat Jun 27 2015 PixelDrift.NET <support@pixeldrift.net> - 2.0.13-1
- Update to v2.0.13

* Sun Jun 21 2015 PixelDrift.NET <support@pixeldrift.net> - 2.0.12-1
- Update to v2.0.12
- Removed build patch for git as fix merged upstream

* Sat Jun 20 2015 PixelDrift.NET <support@pixeldrift.net> - 2.0.11-5
- Added patch to etcd build so build no longer requires git
- Relocated log file to /var/log/etcd.log

* Mon Jun 15 2015 PixelDrift.NET <support@pixeldrift.net> - 2.0.11-4
- Removed Fedora specific patch and Fedora references in spec file

* Sun Jun 14 2015 PixelDrift.NET <support@pixeldrift.net> - 2.0.11-3
- Forked from fc22 repository and built against epel 6 golang
- Removed references to systemd

* Fri May 22 2015 jchaloup <jchaloup@redhat.com> - 2.0.11-2
- ETCD_ADVERTISE_CLIENT_URLS has to be set if ETCD_LISTEN_CLIENT_URLS is
  related: #1222416

* Mon May 18 2015 jchaloup <jchaloup@redhat.com> - 2.0.11-1
- Update to v2.0.11
  resolves: #1222416

* Thu Apr 23 2015 jchaloup <jchaloup@redhat.com> - 2.0.10-1
- Update to v2.0.10
  resolves: #1214705

* Wed Apr 08 2015 jchaloup <jchaloup@redhat.com> - 2.0.9-1
- Update to v2.0.9
  resolves: #1209666

* Fri Apr 03 2015 jchaloup <jchaloup@redhat.com> - 2.0.8-0.2
- Update spec file to fit for rhel too (thanks to eparis)
  related: #1207881

* Wed Apr 01 2015 jchaloup <jchaloup@redhat.com> - 2.0.8-0.1
- Update to v2.0.8
  resolves: #1207881

* Tue Mar 31 2015 jchaloup <jchaloup@redhat.com> - 2.0.7-0.1
- Update to v2.0.7
  Add Godeps.json to doc
  related: #1191441

* Thu Mar 12 2015 jchaloup <jchaloup@redhat.com> - 2.0.5-0.1
- Bump to 9481945228b97c5d019596b921d8b03833964d9e (v2.0.5)

* Tue Mar 10 2015 Eric Paris <eparis@redhat.com> - 2.0.3-0.2
- Fix .service files to work if no config file

* Fri Feb 20 2015 jchaloup <jchaloup@redhat.com> - 2.0.3-0.1
- Bump to upstream 4d728cc8c488a545a8bdeafd054d9ccc2bfb6876

* Wed Feb 18 2015 jchaloup <jchaloup@redhat.com> - 2.0.1-0.2
- Update configuration and service file
  Fix depricated ErrWrongType after update of gogo/protobuf
  related: #1191441

* Wed Feb 11 2015 jchaloup <jchaloup@redhat.com> - 2.0.1-0.1
- Update to 2.0.1
  resolves: #1191441

* Mon Feb 09 2015 jchaloup <jchaloup@redhat.com> - 2.0.0-0.5
- Add missing debug info to binaries (patch from Jan Kratochvil)
  resolves: #1184257

* Fri Jan 30 2015 jchaloup <jchaloup@redhat.com> - 2.0.0-0.4
- Update to etcd-2.0.0
- use gopath as the last directory to search for source code
  related: #1176138

* Mon Jan 26 2015 jchaloup <jchaloup@redhat.com> - 2.0.0-0.3.rc1
- default to /var/lib/etcd/default.etcd as 2.0 uses that default (f21 commit byt eparis)
  related: #1176138
  fix /etc/etcd/etcd.conf path

* Tue Jan 20 2015 jchaloup <jchaloup@redhat.com> - 2.0.0-0.2.rc1
- Update of BuildRequires/Requires, Provides and test
  Add BuildRequire on jonboulle/clockwork
  related: #1176138

* Tue Dec 23 2014 Lokesh Mandvekar <lsm5@fedoraproject.org> - 2.0.0-0.1.rc1
- Resolves: rhbz#1176138 - update to v2.0.0-rc1
- do not redefine gopath
- use jonboulle/clockwork from within Godeps

* Fri Oct 17 2014 jchaloup <jchaloup@redhat.com> - 0.4.6-7
- Add ExclusiveArch for go_arches

* Mon Oct 06 2014 jchaloup <jchaloup@redhat.com> - 0.4.6-6
- related: #1047194
  Remove dependency on go.net

* Mon Oct 06 2014 jchaloup <jchaloup@redhat.com> - 0.4.6-5
- Fix the .service file so it can launch!
  related: #1047194

* Mon Sep 22 2014 jchaloup <jchaloup@redhat.com> - 0.4.6-4
- resolves: #1047194
  Update to 0.4.6 from https://github.com/projectatomic/etcd-package

* Tue Aug 19 2014 Adam Miller <maxamillion@fedoraproject.org> - 0.4.6-3
- Add devel sub-package

* Wed Aug 13 2014 Eric Paris <eparis@redhat.com> - 0.4.6-2
- Bump to 0.4.6
- run as etcd, not root

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Oct 20 2013 Peter Lemenkov <lemenkov@gmail.com> - 0.1.2-5
- goprotobuf library unbundled (see rhbz #1018477)
- go-log library unbundled (see rhbz #1018478)
- go-raft library unbundled (see rhbz #1018479)
- go-systemd library unbundled (see rhbz #1018480)
- kardianos library unbundled (see rhbz #1018481)

* Sun Oct 13 2013 Peter Lemenkov <lemenkov@gmail.com> - 0.1.2-4
- go.net library unbundled (see rhbz #1018476)

* Sat Oct 12 2013 Peter Lemenkov <lemenkov@gmail.com> - 0.1.2-3
- Prepare for packages unbundling
- Verbose build

* Sat Oct 12 2013 Peter Lemenkov <lemenkov@gmail.com> - 0.1.2-2
- Fix typo in the etc.service file

* Sat Oct 12 2013 Peter Lemenkov <lemenkov@gmail.com> - 0.1.2-1
- Ver. 0.1.2
- Integrate with systemd

* Mon Aug 26 2013 Luke Cypret <cypret@fedoraproject.org> - 0.1.1-1
- Initial creation
